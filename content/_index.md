---
title: Homepage
language: en
slug: /
---

# HackBach.nz

| | |
|-|-|
| Hacker | / ˈhæk ər / an intensely curious human, who enjoys understanding and modifying the world beyond the designed intention into something extraordinary - Hippie Hacker |
| Bach | / bætʃ / a New Zealand term for a small weekend or vacation house or shack that is on the beach |

---
An [ii.nz](https://ii.nz) project | hackbach@ii.coop
